package com.trade.portfolio;

import java.util.HashMap;
import java.util.List;

import org.bson.Document;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/portfolio")
public class PortfolioController {

    @Autowired
    private PortfolioService portfolioService;

    @GetMapping(value = "/")
    public HashMap<String, List<String>> getPortfolios() {
        return portfolioService.getPortfolios();
    }

    @GetMapping(value = "/holdings")
    public Document getHoldings() {
        return portfolioService.getHoldings();
    }

    @PostMapping(value = "/addTrade")
    public ResponseEntity<?> addTrade(@RequestBody Trade trade) {
        String message = portfolioService.addTrade(trade);
        return new ResponseEntity(message, HttpStatus.OK);
    }

    @PutMapping(value = "/updateTrade/{id}")
    public ResponseEntity<?> updateTrade(@RequestBody Trade trade, @PathVariable String id) {
        String message = portfolioService.updateTrade(trade, id);
        return new ResponseEntity(message, HttpStatus.OK);
    }

    @DeleteMapping(value = "/removeTrade/{id}")
    public ResponseEntity<?> deleteTrade(@PathVariable String id) {
        String message = portfolioService.deleteTrade(id);
        return new ResponseEntity(message, HttpStatus.OK);
    }

}