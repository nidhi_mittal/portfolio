package com.trade.portfolio;

import java.util.HashMap;
import java.util.List;

import org.bson.Document;

import org.springframework.stereotype.Service;

@Service
public interface PortfolioService {

    HashMap<String, List<String>> getPortfolios();

    String addTrade(Trade trade);

    String updateTrade(Trade trade, String id);

    String deleteTrade(String id);

    Document getHoldings();
}