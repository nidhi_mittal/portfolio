package com.trade.portfolio;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Service;
import org.springframework.data.mongodb.core.MongoTemplate;

@Service
public class PortfolioServiceImp implements PortfolioService {

    @Autowired
    private PortfolioRepository portfolioRepository;

    @Resource(name = "mongoTemplate")
    MongoTemplate mongoTemplate;

    @Override
    public HashMap<String, List<String>> getPortfolios() {
        List tradeList = portfolioRepository.findAll();
        HashMap<String, List<String>> hmstock = Trade.groupByStock(tradeList);
        for (Map.Entry mapElement : hmstock.entrySet()) {
            String key = (String) mapElement.getKey();
            List<String> value = (List<String>) mapElement.getValue();
        }
        return hmstock;
    }

    @Override
    public String addTrade(Trade trade) {
        Trade tradeEx = new Trade(trade.getStock(), trade.getQuantity(), trade.getDate());
        ExampleMatcher matcher = ExampleMatcher.matchingAll();
        Example<Trade> example = Example.of(tradeEx, matcher);
        List<Trade> tradeList = portfolioRepository.findAll(example);
        if (tradeList.size() < 1) {
            portfolioRepository.save(trade);
            return "Trade added successfully!";
        }
        return "Trade already exists, Duplicate trades not allowed!";

    }

    @Override
    public String updateTrade(Trade trade, String id) {
        Trade tradeEx = new Trade(trade.getStock(), trade.getQuantity(), trade.getDate());
        ExampleMatcher matcher = ExampleMatcher.matchingAll();
        Example<Trade> example = Example.of(tradeEx, matcher);
        List<Trade> tradeList = portfolioRepository.findAll(example);

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        Update update = new Update();
        update.set("stock", trade.getStock());
        update.set("quantity", trade.getQuantity());
        update.set("date", trade.getDate());
        update.set("type", trade.getType());
        update.set("price", trade.getPrice());
        if (tradeList.size() < 1) {
            mongoTemplate.findAndModify(query, update, Trade.class);
            return "Trade updated successfully!";
        }
        return "Duplicate trades not allowed!";
    }

    @Override
    public String deleteTrade(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        mongoTemplate.remove(query, Trade.class);
        return "Trade deleted successfully!";
    }

    @Override
    public Document getHoldings() {
        MatchOperation matchOperation = match(new Criteria("type").is("BUY"));
        GroupOperation groupOperation = group("stock").avg("quantity").as("quantity").avg("price").as("price");
        Aggregation aggregation = newAggregation(groupOperation, matchOperation);
        AggregationResults result = mongoTemplate.aggregate(aggregation, "zips", Trade.class);
        Document resultList = result.getRawResults();

        return resultList;
    }

}