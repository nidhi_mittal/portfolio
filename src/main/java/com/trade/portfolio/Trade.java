package com.trade.portfolio;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "trades")
public class Trade {
    private String stock;
    private String type;
    private Integer quantity;
    private Integer price;
    private Date date;

    public Trade(String stock, Integer quantity, Date date) {
        this.setStock(stock);
        this.setQuantity(quantity);
        this.setDate(date);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public static HashMap<String, List<String>> groupByStock(List<Trade> trade) {

        HashMap<String, List<String>> hashMapStock = new HashMap<String, List<String>>();

        for (Trade t : trade) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String strDate = formatter.format(t.date);
            String newTrade = t.type + " " + t.quantity + "@" + t.price + " " + strDate;

            if (!hashMapStock.containsKey(t.stock)) {
                List<String> list = new ArrayList<String>();
                list.add(newTrade);
                hashMapStock.put(t.stock, list);
            } else {
                hashMapStock.get(t.stock).add(newTrade);
            }
        }
        return hashMapStock;
    }

}